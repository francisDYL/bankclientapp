import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpParams, HttpRequest} from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError} from 'rxjs/operators';
import {API_URL} from '../config/api';
import {HttpHeaders} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  userDetails = new Subject<any>();
  userAgency = new Subject<any>();
  userAccount = new Subject<any>();
  accountTransfer = new Subject<any>();
  accountRefill = new Subject<any>();

  constructor(private http: HttpClient) { }

  fetchData(): Observable<HttpResponse<any>> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer ' + localStorage.getItem('token').trim());

    return this.http.get<any>(API_URL + '/clients/' + localStorage.getItem('id'), {headers: headers, observe: 'response'})
                    .pipe(
                         catchError(this.errorHandler)
                    );

  }

  transfer(receiver, amount, account): Observable<HttpResponse<any>> {
      let headers = new HttpHeaders();
      headers = headers.set('Authorization', 'Bearer ' + localStorage.getItem('token').trim());

      return this.http.get<any>(API_URL + '/clients/' + receiver + '/' + account + '/' + amount, {headers: headers, observe: 'response'})
                      .pipe(
                          catchError(this.errorHandler)
                      );
  }

  getUserDetails(): Observable<any> {
    return this.userDetails.asObservable();
  }

  setUserDetails(userDetails) {
    this.userDetails.next(userDetails);
  }

  getUserAgency(): Observable<any> {
    return this.userAgency.asObservable();
  }

  setUserAgency(userAgency) {
    this.userAgency.next(userAgency);
  }

  getUserAccount(): Observable<any> {
    return this.userAccount.asObservable();
  }

  setUserAccount(userAccount) {
    this.userAccount.next(userAccount);
  }

  getAccountTransfer(): Observable<any> {
    return this.accountTransfer.asObservable();
  }

  setAccountTransfer(accountTransfer) {
    this.accountTransfer.next(accountTransfer);
  }

  getAccountRefill(): Observable<any> {
    return this.accountRefill.asObservable();
  }

  setAccountRefill(accountRefill) {
    this.accountRefill.next(accountRefill);
  }

  private errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
