import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../auth/login.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: String;
  password: String;
  error = [];

  constructor(private loginService: LoginService) { }

  signIn() {
    this.hideError();
    this.loginService.signIn(this.login, this.password).subscribe(
      (data: HttpResponse<any>) => this.successHandler(data),
      (error: HttpErrorResponse) => this.errorHandler(error)
    );
  }

  errorHandler(error: HttpErrorResponse) {
    this.login = '';
    this.password = '';
    if (error.status !== 403) {
      this.showError(`can't reach the server`);
    } else {
      this.showError(`wrong login/password`);
    }
}

successHandler(data: HttpResponse<any>) {
  const token = data.headers.get('Authorization').split(' ')[1];
  const id = data.body.pk;
  this.loginService.login(token, id);
}

showError(message) {
  this.error.push({severity: 'error', summary: message});
}

hideError() {
  this.error = [];
}

  ngOnInit() {
  }

}
