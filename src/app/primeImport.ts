import {ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';
import {InputTextModule} from 'primeng/inputtext';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {TableModule} from 'primeng/table';
export const PrimeModule = [
    ButtonModule,
    CardModule,
    InputTextModule,
    MessagesModule,
    MessageModule,
    TableModule
];
