import { Component, OnInit} from '@angular/core';
import {LoginService} from '../auth/login.service';
import {DataService} from '../services/data.service';
import {HttpResponse, HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  error = [];
  login;
  constructor(private loginService: LoginService, private dataService: DataService) {

  }

  ngOnInit() {
    this.dataService.fetchData().subscribe(
      (data: HttpResponse<any>) => this.successHandler(data),
      (error: HttpErrorResponse) => this.errorHandler(error)
    );
  }

  logout() {
    this.loginService.logout();
  }


  errorHandler(error: HttpErrorResponse) {
    console.log(error.status);
    if (error.status !== 403) {
      this.showError(`An error occur Please refresh the page`);
    } else {
      this.showError(`An error occur Please refresh the page`);
    }
}

successHandler(data: HttpResponse<any>) {
  const userDetails = {
    firstName: data.body.firstName,
    lastName: data.body.lastName,
    email: data.body.email,
    phoneNumber: data.body.phoneNumber,
    login: data.body.login,
    pk: data.body.pk,
    birthdate: data.body.birthdate
  };
  this.login = userDetails.login;

  const userAgency = {
    name: data.body.agency.name,
    location: data.body.agency.location,
    email: data.body.agency.email,
    phoneNumber: data.body.agency.phoneNumber,
    pk: data.body.agency.pk
  };

  const userAccount = {
    rib: data.body.account.rib,
    balance: data.body.account.balance,
    pk: data.body.account.pk
  };

  const accountTransfers = [];

  for (let i = 0; i < data.body.account.transfers.length; i++) {
    if (data.body.account.transfers[i].receiver.rib === undefined) {
      const t = this.findRib(data.body.account.transfers, data.body.account.transfers[i].receiver);
      accountTransfers.push(
        {
          amount: data.body.account.transfers[i].amount,
          date: data.body.account.transfers[i].date,
          receiver: t.receiver.rib
        });
    } else {
        accountTransfers.push(
        {
          amount: data.body.account.transfers[i].amount,
          date: data.body.account.transfers[i].date,
          receiver: data.body.account.transfers[i].receiver.rib
        });
    }

  }

  const accountRefills = [];
  for (let i = 0; i < data.body.account.refills.length; i++) {
    accountRefills.push(
      {
        amount: data.body.account.refills[i].amount,
        date: data.body.account.refills[i].date,
        receiver: data.body.account.refills[i].reveiverNumber
      }
    );
  }

  this.dataService.setUserAccount(userAccount);
  this.dataService.setUserDetails(userDetails);
  this.dataService.setAccountTransfer(accountTransfers);
  this.dataService.setAccountRefill(accountRefills);
  this.dataService.setUserAgency(userAgency);

  /*console.log(accountRefills);
  console.log(accountTransfers);
  console.log(userAccount);
  console.log(userAgency);
  console.log(userDetails);
  console.log(data);*/
}

findRib(list = [], id) {
  return list.find(element => element.receiver.id === id);
}


showError(message) {
  this.error.push({severity: 'error', summary: message});
}

hideError() {
  this.error = [];
}

}
