import { Component, OnInit, OnDestroy } from '@angular/core';
import {DataService} from '../../services/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  account = {rib: null, balance: null};
  transfers = [];
  refills = [];

  transfersSubscription: Subscription;
  refillsSubscription: Subscription;
  accountSubscription: Subscription;

  constructor(private dataService: DataService) {
    this.refillsSubscription = this.dataService.getAccountRefill().subscribe(refills => this.refills = refills);
    this.transfersSubscription = this.dataService.getAccountTransfer().subscribe(transfers => this.transfers = transfers);
    this.accountSubscription = this.dataService.getUserAccount().subscribe(account => this.account = account);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.transfersSubscription.unsubscribe();
    this.refillsSubscription.unsubscribe();
    this.accountSubscription.unsubscribe();
  }
}
