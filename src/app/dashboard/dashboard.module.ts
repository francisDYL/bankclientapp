import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule } from '@angular/forms';
import { DashboardRoutingModule, RoutingComponents } from './dashboard-routing.module';
import { PrimeModule } from '../primeImport';

@NgModule({
  declarations: [RoutingComponents],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    PrimeModule
  ]
})
export class DashboardModule { }
