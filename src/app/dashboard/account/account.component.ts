import { Component, OnInit, OnDestroy } from '@angular/core';
import {DataService} from '../../services/data.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})

export class AccountComponent implements OnInit, OnDestroy {

  userDetails;
  userAgency;

  userDetailsSubscription: Subscription;
  userAgencySubscription: Subscription;

  constructor(private dataService: DataService) {
    this.userDetailsSubscription = this.dataService.getUserDetails().subscribe(
      user => this.userDetails = user);
    this.userAgencySubscription = this.dataService.getUserAgency().subscribe(
      agency => this.userAgency = agency
    );
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.userDetailsSubscription.unsubscribe();
    this.userAgencySubscription.unsubscribe();
  }
}
