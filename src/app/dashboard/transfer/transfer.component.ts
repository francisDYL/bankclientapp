import { Component, OnInit, OnDestroy } from '@angular/core';
import {DataService} from '../../services/data.service';
import { Subscription } from 'rxjs';
import {HttpResponse, HttpErrorResponse} from '@angular/common/http';
@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit, OnDestroy {

  /*recipient: number;*/
  recipient = 123456798;
  amount: number;
  error = [];

  account = {rib: null, balance: null, pk: null};
  transfers = [];
  refills = [];

  transfersSubscription: Subscription;
  refillsSubscription: Subscription;
  accountSubscription: Subscription;


  constructor(private dataService: DataService) {
    this.refillsSubscription = this.dataService.getAccountRefill().subscribe(refills => this.refills = refills);
    this.transfersSubscription = this.dataService.getAccountTransfer().subscribe(transfers => this.transfers = transfers);
    this.accountSubscription = this.dataService.getUserAccount().subscribe(account => this.account = account);
  }

  transfer() {
    this.dataService.transfer(this.recipient, this.amount, this.account.pk).subscribe(
      (data: HttpResponse<any>) => this.successHandler(data),
      (error: HttpErrorResponse) => this.errorHandler(error)
    );
  }

  errorHandler(error: HttpErrorResponse) {
    if (error.status === 404) {
      this.showError('the is no account with number ' + this.recipient, 'error');
    } else {
      this.showError('An error occur Please refresh the page', 'error');
    }
    this.amount = null;
    /*this.recipient = null;*/
}

successHandler(data: HttpResponse<any>) {
  this.amount = null;
  /*this.recipient = null;*/
  this.showError('transfer well done', 'success');
}

  ngOnInit() {
  }

  ngOnDestroy() {
    this.transfersSubscription.unsubscribe();
    this.refillsSubscription.unsubscribe();
    this.accountSubscription.unsubscribe();
  }

  showError(message, severity) {
    this.error.push({severity: severity, summary: message});
  }

  hideError() {
    this.error = [];
  }

}
