import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RouteGuard} from './auth/route.guard';

const routes: Routes = [
  {path: '', redirectTo: 'welcome', pathMatch: 'full'},
  {path: 'welcome', component: LoginComponent},
  {
    path: 'dashboard',
    canActivate: [RouteGuard],
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  }
  /*{
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  }*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
