import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError} from 'rxjs/operators';
import {API_URL} from '../config/api';
import { Router } from '@angular/router';
import {HttpHeaders} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private user = new Subject<any>();

  constructor(private http: HttpClient, private router: Router) { }


  signIn(login, password): Observable<HttpResponse<any>> {
    return this.http.post<any>(API_URL + '/login', {login, password}, {observe: 'response'})
                    .pipe(
                         catchError(this.errorHandler)
                    );
  }

  isAuthenticated(): boolean {
    if (localStorage.getItem('token') != null) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.clear();

    this.router.navigate(['/welcome']);
  }

  login(token, userId) {
    localStorage.setItem('token', token);
    localStorage.setItem('id', userId);
    this.router.navigate(['/dashboard']);
  }

  private errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
}
